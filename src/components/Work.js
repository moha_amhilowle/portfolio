import React from "react";
import { Link } from "react-router-dom";
import { useMediaQuery } from "react-responsive";

function Work() {
  const isMobile = useMediaQuery({ query: "(max-width: 500px)" });
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-device-width: 1224px)",
  });
  return (
    <div style={workStyles}>
      {isDesktopOrLaptop && (
        <div style={androidStyle}>
          <h1 style={projectsHeader}>Android projects</h1>
          <p style={workDesc}>Here are some of my Android apps</p>
          <Link style={viewWorkBtn} className="workBtn" to="/android">
            VIEW MY PROJECTS
          </Link>
        </div>
      )}
      {isMobile && (
        <div style={phoneAndroidStyle}>
          <h1 style={projectsHeader}>Android projects</h1>
          <p style={workDesc}>Here are some of my Android apps</p>
          <Link style={viewWorkBtn} className="workBtn" to="/android">
            VIEW MY PROJECTS
          </Link>
        </div>
      )}
      {isDesktopOrLaptop && <div style={divider}></div>}
      {isDesktopOrLaptop && (
        <div style={webStyle}>
          <h1 style={projectsHeader}>Web projects</h1>
          <p style={workDesc}>Take a look at some of my websites</p>
          <Link style={viewWorkBtn} className="workBtn" to="/web">
            VIEW MY PROJECTS
          </Link>
        </div>
      )}
      {isMobile && (
        <div style={phoneWebStyle}>
          <h1 style={projectsHeader}>Web projects</h1>
          <p style={workDesc}>Take a look at some of my websites</p>
          <Link style={viewWorkBtn} className="workBtn" to="/web">
            VIEW MY PROJECTS
          </Link>
        </div>
      )}
    </div>
  );
}

const workStyles = {
  minHeight: "100vh",
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
};

const divider = {
  borderLeft: "1px solid #4831d4",
  borderRight: "1px solid #4831d4",
  height: "400px",
  marginLeft: "180px",
  marginRight: "180px",
  marginTop: "60px",
};

const androidStyle = {
  paddingTop: "100px",
  paddingLeft: "150px",
};

const phoneAndroidStyle = {
  paddingTop: "100px",
  paddingLeft: "50px",
};

const webStyle = {
  paddingTop: "100px",
};
const phoneWebStyle = {
  paddingTop: "100px",
  paddingLeft: "50px",
};

const viewWorkBtn = {
  border: "1px solid  #4831d4",
  padding: "12px 60px 12px 60px",
  fontFamily: "Lato",
};

const projectsHeader = {
  color: "#4831d4",
  fontSize: "40px",
  fontWeight: "900",
};

const workDesc = {
  marginBottom: "100px",
  fontFamily: "Lato",
  fontSize: "16px",
};

export default Work;
