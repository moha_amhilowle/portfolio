import React from "react";
import { Link } from "react-router-dom";

import "../index.css";

export default ({ close }) => (
  <div className="menu">
    <ul>
      <li>
        <Link onClick={close} activeclassname="current" to="/">
          Home
        </Link>
      </li>
      <li>
        <Link onClick={close} activeclassname="current" to="android">
          Android Projects
        </Link>
      </li>
      <li>
        <Link onClick={close} activeclassname="current" to="web">
          Web Projects
        </Link>
      </li>
    </ul>
  </div>
);
