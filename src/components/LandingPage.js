import React from "react";
import ReactFullpage from "@fullpage/react-fullpage";
import "fullpage.js/vendors/scrolloverflow";

import About from "./About";
import Contact from "./Contact";
import Footer from "./Footer";
import HomeContent from "./HomeContent";
import Work from "./Work";

const anchors = [];

const LandingPage = () => (
  <ReactFullpage
    anchors={anchors}
    scrollingSpeed={1000}
    navigation
    navigationTooltips={anchors}
    sectionsColor={["#282c34", "#4831d4"]}
    render={({ state, fullpageApi }) => {
      return (
        <div>
          <div className="section">
            <HomeContent />
            <div className="explorebtnWrapper">
              <button
                className="exploreBtn"
                onClick={() => fullpageApi.moveSectionDown()}
              >
                <br />
                <i className="icon arrow down"></i>
              </button>
            </div>
          </div>
          <div className="section">
            <About />
          </div>
          <div className="section">
            <Work />
          </div>
          <div className="section">
            <Contact />
          </div>
          <div className="section">
            <Footer />
          </div>
        </div>
      );
    }}
  />
);

export default LandingPage;
