import React from "react";
import * as typeformEmbed from "@typeform/embed";

class Contact extends React.Component {
  constructor() {
    super();
    this.element = null;
  }

  componentDidMount() {
    if (this.element) {
      typeformEmbed.makeWidget(
        this.element,
        "https://victorteka.typeform.com/to/uzUzlUKG",
        {
          hideFooter: true,
          hideHeaders: true,
          opacity: 0,
        }
      );
    }
  }

  render() {
    return (
      <div style={{ width: "100%", height: "100vh" }}>
        {/* <h1>Contact section</h1> */}
        <div
          style={{ width: "100%", height: "90vh" }}
          ref={(element) => (this.element = element)}
        />
      </div>
    );
  }
}

export default Contact;
